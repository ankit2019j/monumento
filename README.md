Monumento is an application where people can detect monuments, view and add to their collection and further use the AR tiles in their own spaces. It uses Machine Learning and geo-fencing the location of the person to identify the monuments, once the monument is detected the user can see its 3D as well as AR view as well as know its info and history of the monument. User unlocks each monuments AR tile as the user visits new monuments. These  AR tiles can be used by the user anywhere for a specified period of time. The user also earns Badges by visiting these monuments which can be shared. There will be a World leaderboard maintained of all the users.
***SEE*** -  See the monuments and Capture them, learn about their construction, history and much more.
***Tile*** - Once you see a monument, you unlock its AR tile which you can use for a specified period of time.
***Flaunt*** -So take pictures with the Tiles, flaunt it in Leaderboard and share it with your friends.


[**PROTOTYPE VIDEO**](https://youtu.be/R9UKXR3RYTE)  (UI PROTOTYPE VIDEO)(https://youtu.be/R9UKXR3RYTE)

**APPLICATION GIF**(progress till now)(**Please wait for the gif to load if its not loading open ss folder**)
![APPLICATION GIF](ss/demo2.gif)



**mockup ui**



![**splash**](mockupUI/Splash.png) ![**splash**](mockupUI/info.png) ![**splash**](mockupUI/profile.png) ![**splash**](mockupUI/arview.png) ![**splash**](mockupUI/badges.png)



